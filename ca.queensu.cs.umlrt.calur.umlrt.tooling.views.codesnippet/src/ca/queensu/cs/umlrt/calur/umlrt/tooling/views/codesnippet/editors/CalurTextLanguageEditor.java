package ca.queensu.cs.umlrt.calur.umlrt.tooling.views.codesnippet.editors;

import org.eclipse.core.databinding.observable.ChangeEvent;
import org.eclipse.core.databinding.observable.Diffs;
import org.eclipse.core.databinding.observable.IChangeListener;
import org.eclipse.core.databinding.observable.value.ValueDiff;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.papyrus.infra.widgets.databinding.StyledTextObservableValue;
import org.eclipse.papyrus.uml.properties.expression.ExpressionList.Expression;
import org.eclipse.papyrus.uml.xtext.integration.StyledTextXtextAdapter;
import org.eclipse.papyrus.uml.xtext.integration.core.IXtextFakeContextResourcesProvider;
import org.eclipse.papyrusrt.umlrt.core.utils.ModelUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.uml2.uml.Element;
import org.eclipse.xtext.parser.IParseResult;
import org.eclipse.xtext.validation.IResourceValidator;
import org.eclipse.xtext.validation.ResourceValidatorImpl;

import com.google.inject.Injector;

import ca.queensu.cs.umlrt.calur.CalurDerivedStateComputer;
import ca.queensu.cs.umlrt.calur.calur.Action;
import ca.queensu.cs.umlrt.calur.generator.CalurGenerator;
import ca.queensu.cs.umlrt.calur.ui.internal.CalurActivator;
import ca.queensu.cs.umlrt.calur.validation.CalurValidator;


public class CalurTextLanguageEditor extends AbstractLanguageEditor {

	private StyledTextObservableValue styledTextObservable;
	private StyledText styledText;
	private StyledTextXtextAdapter xtextAdapter;
	private CalurGenerator generator;
	private String output = "";
	
	@Override
	protected void createContents(Composite parent) {
		
		CalurActivator activator = CalurActivator.getInstance();
		Injector injector = activator.getInjector(CalurActivator.CA_QUEENSU_CS_UMLRT_CALUR_CALUR);

		generator = injector.getInstance(CalurGenerator.class);
		CalurValidator validator = injector.getInstance(CalurValidator.class);
		IResourceValidator validatorResource = injector.getInstance(IResourceValidator.class);
		CalurDerivedStateComputer derivedStateComputer = injector.getInstance(CalurDerivedStateComputer.class);
		
		styledText = new StyledText(parent, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL);
		styledText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		styledText.setFont(JFaceResources.getTextFont());
		styledText.layout();

		// adapt to xtext
		xtextAdapter = new StyledTextXtextAdapter(injector, IXtextFakeContextResourcesProvider.NULL_CONTEXT_PROVIDER);
		xtextAdapter.adapt(styledText);

		styledTextObservable = new StyledTextObservableValue(styledText, null, SWT.FocusOut);

		styledTextObservable.addChangeListener(new IChangeListener() {

			@Override
			public void handleChange(ChangeEvent event) {

				IParseResult xtextParseResult = xtextAdapter.getXtextParseResult();
				EObject root = xtextParseResult.getRootASTElement();

				if (styledText.getText().equals(expression.getBody())) {
					// ignore no change
					return;
				}

				Expression oldEx = new Expression();
				oldEx.setLanguage(expression.getLanguage());
				oldEx.setBody(expression.getBody());

				// update the body value
				expression.setBody(styledText.getText());

				ValueDiff<Expression> diff = Diffs.createValueDiff(oldEx, expression);

				fireValueChange(diff);
									
//				System.out.println("validation:" + validator.validate(root, null, null));

			}
		});
	}

	public String doGetOutputCode() {
		return output;
	}

	@Override
	public void dispose() {
		styledTextObservable.dispose();
		super.dispose();
	}

	@Override
	protected void refresh() {
		if (!styledText.isDisposed()) {
			styledText.setText(expression.getBody() == null ? "" : expression.getBody());
		}
	}

}
