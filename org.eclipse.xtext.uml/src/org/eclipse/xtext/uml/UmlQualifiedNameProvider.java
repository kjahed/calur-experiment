package org.eclipse.xtext.uml;
 
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.naming.SimpleNameProvider;
import org.eclipse.xtext.naming.DefaultDeclarativeQualifiedNameProvider;
import org.eclipse.xtext.naming.IQualifiedNameConverter;
 
public class UmlQualifiedNameProvider extends SimpleNameProvider {
 
	@Override
	public QualifiedName getFullyQualifiedName(EObject obj) {
		// TODO Auto-generated method stub
		if (obj instanceof Transition || obj instanceof State) {
			IQualifiedNameConverter qualifiedNameConverter = new IQualifiedNameConverter.DefaultImpl();
			NamedElement element = (NamedElement)obj;
			
			String qualifiedName = element.getQualifiedName();
			if (qualifiedName == null) {
				if (element instanceof Transition) {
					qualifiedName = ((Transition)element).getSource().getName() + "_to_" + ((Transition)element).getTarget().getName();
				}
				else {
					System.err.println(element.getClass());
				}
			}
			
			QualifiedName qualifiedNameFromConverter = qualifiedNameConverter.toQualifiedName(qualifiedName);
			return qualifiedNameFromConverter;
		}
		QualifiedName name = super.getFullyQualifiedName(obj);
		return name;
	}
	
	@Override
	public QualifiedName apply(EObject from) {
		// TODO Auto-generated method stub
		QualifiedName name =  super.apply(from);
		return name;
	}
}