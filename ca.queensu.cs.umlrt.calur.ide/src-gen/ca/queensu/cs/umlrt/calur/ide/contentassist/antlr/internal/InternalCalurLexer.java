package ca.queensu.cs.umlrt.calur.ide.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalCalurLexer extends Lexer {
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int T__60=60;
    public static final int T__61=61;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__66=66;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__23=23;
    public static final int T__67=67;
    public static final int T__24=24;
    public static final int T__68=68;
    public static final int T__25=25;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__63=63;
    public static final int T__20=20;
    public static final int T__64=64;
    public static final int T__21=21;
    public static final int T__65=65;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__73=73;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__74=74;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int RULE_FLOAT=7;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators

    public InternalCalurLexer() {;} 
    public InternalCalurLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalCalurLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalCalur.g"; }

    // $ANTLR start "T__12"
    public final void mT__12() throws RecognitionException {
        try {
            int _type = T__12;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:11:7: ( 'not' )
            // InternalCalur.g:11:9: 'not'
            {
            match("not"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__12"

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:12:7: ( 'rand' )
            // InternalCalur.g:12:9: 'rand'
            {
            match("rand"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:13:7: ( '<' )
            // InternalCalur.g:13:9: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:14:7: ( '>' )
            // InternalCalur.g:14:9: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:15:7: ( '<=' )
            // InternalCalur.g:15:9: '<='
            {
            match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:16:7: ( '>=' )
            // InternalCalur.g:16:9: '>='
            {
            match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:17:7: ( '=' )
            // InternalCalur.g:17:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:18:7: ( '<>' )
            // InternalCalur.g:18:9: '<>'
            {
            match("<>"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:19:7: ( 'sqrt' )
            // InternalCalur.g:19:9: 'sqrt'
            {
            match("sqrt"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:20:7: ( 'abs' )
            // InternalCalur.g:20:9: 'abs'
            {
            match("abs"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:21:7: ( 'ceil' )
            // InternalCalur.g:21:9: 'ceil'
            {
            match("ceil"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:22:7: ( 'floor' )
            // InternalCalur.g:22:9: 'floor'
            {
            match("floor"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:23:7: ( 'capsuleName' )
            // InternalCalur.g:23:9: 'capsuleName'
            {
            match("capsuleName"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:24:7: ( 'capsulePartName' )
            // InternalCalur.g:24:9: 'capsulePartName'
            {
            match("capsulePartName"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:25:7: ( 'index' )
            // InternalCalur.g:25:9: 'index'
            {
            match("index"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:26:7: ( '+' )
            // InternalCalur.g:26:9: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:27:7: ( '-' )
            // InternalCalur.g:27:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:28:7: ( '*' )
            // InternalCalur.g:28:9: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:29:7: ( '/' )
            // InternalCalur.g:29:9: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:30:7: ( '%' )
            // InternalCalur.g:30:9: '%'
            {
            match('%'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:31:7: ( 'true' )
            // InternalCalur.g:31:9: 'true'
            {
            match("true"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:32:7: ( 'false' )
            // InternalCalur.g:32:9: 'false'
            {
            match("false"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:33:7: ( 'seconds' )
            // InternalCalur.g:33:9: 'seconds'
            {
            match("seconds"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:34:7: ( 'milliseconds' )
            // InternalCalur.g:34:9: 'milliseconds'
            {
            match("milliseconds"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public final void mT__36() throws RecognitionException {
        try {
            int _type = T__36;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:35:7: ( 'microseconds' )
            // InternalCalur.g:35:9: 'microseconds'
            {
            match("microseconds"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "T__37"
    public final void mT__37() throws RecognitionException {
        try {
            int _type = T__37;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:36:7: ( 'verbatim' )
            // InternalCalur.g:36:9: 'verbatim'
            {
            match("verbatim"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__37"

    // $ANTLR start "T__38"
    public final void mT__38() throws RecognitionException {
        try {
            int _type = T__38;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:37:7: ( ';' )
            // InternalCalur.g:37:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__38"

    // $ANTLR start "T__39"
    public final void mT__39() throws RecognitionException {
        try {
            int _type = T__39;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:38:7: ( 'send' )
            // InternalCalur.g:38:9: 'send'
            {
            match("send"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__39"

    // $ANTLR start "T__40"
    public final void mT__40() throws RecognitionException {
        try {
            int _type = T__40;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:39:7: ( 'message' )
            // InternalCalur.g:39:9: 'message'
            {
            match("message"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__40"

    // $ANTLR start "T__41"
    public final void mT__41() throws RecognitionException {
        try {
            int _type = T__41;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:40:7: ( 'to' )
            // InternalCalur.g:40:9: 'to'
            {
            match("to"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__41"

    // $ANTLR start "T__42"
    public final void mT__42() throws RecognitionException {
        try {
            int _type = T__42;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:41:7: ( 'port' )
            // InternalCalur.g:41:9: 'port'
            {
            match("port"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__42"

    // $ANTLR start "T__43"
    public final void mT__43() throws RecognitionException {
        try {
            int _type = T__43;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:42:7: ( 'with' )
            // InternalCalur.g:42:9: 'with'
            {
            match("with"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__43"

    // $ANTLR start "T__44"
    public final void mT__44() throws RecognitionException {
        try {
            int _type = T__44;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:43:7: ( 'parameters' )
            // InternalCalur.g:43:9: 'parameters'
            {
            match("parameters"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__44"

    // $ANTLR start "T__45"
    public final void mT__45() throws RecognitionException {
        try {
            int _type = T__45;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:44:7: ( 'log' )
            // InternalCalur.g:44:9: 'log'
            {
            match("log"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__45"

    // $ANTLR start "T__46"
    public final void mT__46() throws RecognitionException {
        try {
            int _type = T__46;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:45:7: ( 'inform' )
            // InternalCalur.g:45:9: 'inform'
            {
            match("inform"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__46"

    // $ANTLR start "T__47"
    public final void mT__47() throws RecognitionException {
        try {
            int _type = T__47;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:46:7: ( 'in' )
            // InternalCalur.g:46:9: 'in'
            {
            match("in"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__47"

    // $ANTLR start "T__48"
    public final void mT__48() throws RecognitionException {
        try {
            int _type = T__48;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:47:7: ( ':' )
            // InternalCalur.g:47:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__48"

    // $ANTLR start "T__49"
    public final void mT__49() throws RecognitionException {
        try {
            int _type = T__49;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:48:7: ( 'and' )
            // InternalCalur.g:48:9: 'and'
            {
            match("and"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__49"

    // $ANTLR start "T__50"
    public final void mT__50() throws RecognitionException {
        try {
            int _type = T__50;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:49:7: ( 'at' )
            // InternalCalur.g:49:9: 'at'
            {
            match("at"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__50"

    // $ANTLR start "T__51"
    public final void mT__51() throws RecognitionException {
        try {
            int _type = T__51;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:50:7: ( 'every' )
            // InternalCalur.g:50:9: 'every'
            {
            match("every"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__51"

    // $ANTLR start "T__52"
    public final void mT__52() throws RecognitionException {
        try {
            int _type = T__52;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:51:7: ( 'cancel' )
            // InternalCalur.g:51:9: 'cancel'
            {
            match("cancel"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__52"

    // $ANTLR start "T__53"
    public final void mT__53() throws RecognitionException {
        try {
            int _type = T__53;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:52:7: ( 'incarnate' )
            // InternalCalur.g:52:9: 'incarnate'
            {
            match("incarnate"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__53"

    // $ANTLR start "T__54"
    public final void mT__54() throws RecognitionException {
        try {
            int _type = T__54;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:53:7: ( 'destroy' )
            // InternalCalur.g:53:9: 'destroy'
            {
            match("destroy"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__54"

    // $ANTLR start "T__55"
    public final void mT__55() throws RecognitionException {
        try {
            int _type = T__55;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:54:7: ( 'import' )
            // InternalCalur.g:54:9: 'import'
            {
            match("import"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__55"

    // $ANTLR start "T__56"
    public final void mT__56() throws RecognitionException {
        try {
            int _type = T__56;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:55:7: ( 'deport' )
            // InternalCalur.g:55:9: 'deport'
            {
            match("deport"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__56"

    // $ANTLR start "T__57"
    public final void mT__57() throws RecognitionException {
        try {
            int _type = T__57;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:56:7: ( 'register' )
            // InternalCalur.g:56:9: 'register'
            {
            match("register"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__57"

    // $ANTLR start "T__58"
    public final void mT__58() throws RecognitionException {
        try {
            int _type = T__58;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:57:7: ( 'deregister' )
            // InternalCalur.g:57:9: 'deregister'
            {
            match("deregister"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__58"

    // $ANTLR start "T__59"
    public final void mT__59() throws RecognitionException {
        try {
            int _type = T__59;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:58:7: ( '(' )
            // InternalCalur.g:58:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__59"

    // $ANTLR start "T__60"
    public final void mT__60() throws RecognitionException {
        try {
            int _type = T__60;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:59:7: ( ')' )
            // InternalCalur.g:59:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__60"

    // $ANTLR start "T__61"
    public final void mT__61() throws RecognitionException {
        try {
            int _type = T__61;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:60:7: ( 'var' )
            // InternalCalur.g:60:9: 'var'
            {
            match("var"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__61"

    // $ANTLR start "T__62"
    public final void mT__62() throws RecognitionException {
        try {
            int _type = T__62;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:61:7: ( 'call' )
            // InternalCalur.g:61:9: 'call'
            {
            match("call"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__62"

    // $ANTLR start "T__63"
    public final void mT__63() throws RecognitionException {
        try {
            int _type = T__63;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:62:7: ( 'return' )
            // InternalCalur.g:62:9: 'return'
            {
            match("return"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__63"

    // $ANTLR start "T__64"
    public final void mT__64() throws RecognitionException {
        try {
            int _type = T__64;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:63:7: ( 'srand' )
            // InternalCalur.g:63:9: 'srand'
            {
            match("srand"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__64"

    // $ANTLR start "T__65"
    public final void mT__65() throws RecognitionException {
        try {
            int _type = T__65;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:64:7: ( ':=' )
            // InternalCalur.g:64:9: ':='
            {
            match(":="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__65"

    // $ANTLR start "T__66"
    public final void mT__66() throws RecognitionException {
        try {
            int _type = T__66;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:65:7: ( 'this.' )
            // InternalCalur.g:65:9: 'this.'
            {
            match("this."); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__66"

    // $ANTLR start "T__67"
    public final void mT__67() throws RecognitionException {
        try {
            int _type = T__67;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:66:7: ( '.' )
            // InternalCalur.g:66:9: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__67"

    // $ANTLR start "T__68"
    public final void mT__68() throws RecognitionException {
        try {
            int _type = T__68;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:67:7: ( 'if' )
            // InternalCalur.g:67:9: 'if'
            {
            match("if"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__68"

    // $ANTLR start "T__69"
    public final void mT__69() throws RecognitionException {
        try {
            int _type = T__69;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:68:7: ( 'then' )
            // InternalCalur.g:68:9: 'then'
            {
            match("then"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__69"

    // $ANTLR start "T__70"
    public final void mT__70() throws RecognitionException {
        try {
            int _type = T__70;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:69:7: ( '{' )
            // InternalCalur.g:69:9: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__70"

    // $ANTLR start "T__71"
    public final void mT__71() throws RecognitionException {
        try {
            int _type = T__71;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:70:7: ( '}' )
            // InternalCalur.g:70:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__71"

    // $ANTLR start "T__72"
    public final void mT__72() throws RecognitionException {
        try {
            int _type = T__72;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:71:7: ( 'else' )
            // InternalCalur.g:71:9: 'else'
            {
            match("else"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__72"

    // $ANTLR start "T__73"
    public final void mT__73() throws RecognitionException {
        try {
            int _type = T__73;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:72:7: ( '()' )
            // InternalCalur.g:72:9: '()'
            {
            match("()"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__73"

    // $ANTLR start "T__74"
    public final void mT__74() throws RecognitionException {
        try {
            int _type = T__74;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:73:7: ( 'null' )
            // InternalCalur.g:73:9: 'null'
            {
            match("null"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__74"

    // $ANTLR start "RULE_FLOAT"
    public final void mRULE_FLOAT() throws RecognitionException {
        try {
            int _type = RULE_FLOAT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:7640:12: ( RULE_INT '.' RULE_INT )
            // InternalCalur.g:7640:14: RULE_INT '.' RULE_INT
            {
            mRULE_INT(); 
            match('.'); 
            mRULE_INT(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_FLOAT"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:7642:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // InternalCalur.g:7642:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // InternalCalur.g:7642:11: ( '^' )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0=='^') ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalCalur.g:7642:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalCalur.g:7642:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>='0' && LA2_0<='9')||(LA2_0>='A' && LA2_0<='Z')||LA2_0=='_'||(LA2_0>='a' && LA2_0<='z')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalCalur.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:7644:10: ( ( '0' .. '9' )+ )
            // InternalCalur.g:7644:12: ( '0' .. '9' )+
            {
            // InternalCalur.g:7644:12: ( '0' .. '9' )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>='0' && LA3_0<='9')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalCalur.g:7644:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:7646:13: ( ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // InternalCalur.g:7646:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // InternalCalur.g:7646:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0=='\"') ) {
                alt6=1;
            }
            else if ( (LA6_0=='\'') ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalCalur.g:7646:16: '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // InternalCalur.g:7646:20: ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop4:
                    do {
                        int alt4=3;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0=='\\') ) {
                            alt4=1;
                        }
                        else if ( ((LA4_0>='\u0000' && LA4_0<='!')||(LA4_0>='#' && LA4_0<='[')||(LA4_0>=']' && LA4_0<='\uFFFF')) ) {
                            alt4=2;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // InternalCalur.g:7646:21: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalCalur.g:7646:28: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // InternalCalur.g:7646:48: '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // InternalCalur.g:7646:53: ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop5:
                    do {
                        int alt5=3;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0=='\\') ) {
                            alt5=1;
                        }
                        else if ( ((LA5_0>='\u0000' && LA5_0<='&')||(LA5_0>='(' && LA5_0<='[')||(LA5_0>=']' && LA5_0<='\uFFFF')) ) {
                            alt5=2;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // InternalCalur.g:7646:54: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalCalur.g:7646:61: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:7648:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // InternalCalur.g:7648:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // InternalCalur.g:7648:24: ( options {greedy=false; } : . )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0=='*') ) {
                    int LA7_1 = input.LA(2);

                    if ( (LA7_1=='/') ) {
                        alt7=2;
                    }
                    else if ( ((LA7_1>='\u0000' && LA7_1<='.')||(LA7_1>='0' && LA7_1<='\uFFFF')) ) {
                        alt7=1;
                    }


                }
                else if ( ((LA7_0>='\u0000' && LA7_0<=')')||(LA7_0>='+' && LA7_0<='\uFFFF')) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalCalur.g:7648:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:7650:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // InternalCalur.g:7650:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // InternalCalur.g:7650:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>='\u0000' && LA8_0<='\t')||(LA8_0>='\u000B' && LA8_0<='\f')||(LA8_0>='\u000E' && LA8_0<='\uFFFF')) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalCalur.g:7650:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            // InternalCalur.g:7650:40: ( ( '\\r' )? '\\n' )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0=='\n'||LA10_0=='\r') ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalCalur.g:7650:41: ( '\\r' )? '\\n'
                    {
                    // InternalCalur.g:7650:41: ( '\\r' )?
                    int alt9=2;
                    int LA9_0 = input.LA(1);

                    if ( (LA9_0=='\r') ) {
                        alt9=1;
                    }
                    switch (alt9) {
                        case 1 :
                            // InternalCalur.g:7650:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:7652:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // InternalCalur.g:7652:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // InternalCalur.g:7652:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt11=0;
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0>='\t' && LA11_0<='\n')||LA11_0=='\r'||LA11_0==' ') ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalCalur.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt11 >= 1 ) break loop11;
                        EarlyExitException eee =
                            new EarlyExitException(11, input);
                        throw eee;
                }
                cnt11++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalCalur.g:7654:16: ( . )
            // InternalCalur.g:7654:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // InternalCalur.g:1:8: ( T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | T__69 | T__70 | T__71 | T__72 | T__73 | T__74 | RULE_FLOAT | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt12=71;
        alt12 = dfa12.predict(input);
        switch (alt12) {
            case 1 :
                // InternalCalur.g:1:10: T__12
                {
                mT__12(); 

                }
                break;
            case 2 :
                // InternalCalur.g:1:16: T__13
                {
                mT__13(); 

                }
                break;
            case 3 :
                // InternalCalur.g:1:22: T__14
                {
                mT__14(); 

                }
                break;
            case 4 :
                // InternalCalur.g:1:28: T__15
                {
                mT__15(); 

                }
                break;
            case 5 :
                // InternalCalur.g:1:34: T__16
                {
                mT__16(); 

                }
                break;
            case 6 :
                // InternalCalur.g:1:40: T__17
                {
                mT__17(); 

                }
                break;
            case 7 :
                // InternalCalur.g:1:46: T__18
                {
                mT__18(); 

                }
                break;
            case 8 :
                // InternalCalur.g:1:52: T__19
                {
                mT__19(); 

                }
                break;
            case 9 :
                // InternalCalur.g:1:58: T__20
                {
                mT__20(); 

                }
                break;
            case 10 :
                // InternalCalur.g:1:64: T__21
                {
                mT__21(); 

                }
                break;
            case 11 :
                // InternalCalur.g:1:70: T__22
                {
                mT__22(); 

                }
                break;
            case 12 :
                // InternalCalur.g:1:76: T__23
                {
                mT__23(); 

                }
                break;
            case 13 :
                // InternalCalur.g:1:82: T__24
                {
                mT__24(); 

                }
                break;
            case 14 :
                // InternalCalur.g:1:88: T__25
                {
                mT__25(); 

                }
                break;
            case 15 :
                // InternalCalur.g:1:94: T__26
                {
                mT__26(); 

                }
                break;
            case 16 :
                // InternalCalur.g:1:100: T__27
                {
                mT__27(); 

                }
                break;
            case 17 :
                // InternalCalur.g:1:106: T__28
                {
                mT__28(); 

                }
                break;
            case 18 :
                // InternalCalur.g:1:112: T__29
                {
                mT__29(); 

                }
                break;
            case 19 :
                // InternalCalur.g:1:118: T__30
                {
                mT__30(); 

                }
                break;
            case 20 :
                // InternalCalur.g:1:124: T__31
                {
                mT__31(); 

                }
                break;
            case 21 :
                // InternalCalur.g:1:130: T__32
                {
                mT__32(); 

                }
                break;
            case 22 :
                // InternalCalur.g:1:136: T__33
                {
                mT__33(); 

                }
                break;
            case 23 :
                // InternalCalur.g:1:142: T__34
                {
                mT__34(); 

                }
                break;
            case 24 :
                // InternalCalur.g:1:148: T__35
                {
                mT__35(); 

                }
                break;
            case 25 :
                // InternalCalur.g:1:154: T__36
                {
                mT__36(); 

                }
                break;
            case 26 :
                // InternalCalur.g:1:160: T__37
                {
                mT__37(); 

                }
                break;
            case 27 :
                // InternalCalur.g:1:166: T__38
                {
                mT__38(); 

                }
                break;
            case 28 :
                // InternalCalur.g:1:172: T__39
                {
                mT__39(); 

                }
                break;
            case 29 :
                // InternalCalur.g:1:178: T__40
                {
                mT__40(); 

                }
                break;
            case 30 :
                // InternalCalur.g:1:184: T__41
                {
                mT__41(); 

                }
                break;
            case 31 :
                // InternalCalur.g:1:190: T__42
                {
                mT__42(); 

                }
                break;
            case 32 :
                // InternalCalur.g:1:196: T__43
                {
                mT__43(); 

                }
                break;
            case 33 :
                // InternalCalur.g:1:202: T__44
                {
                mT__44(); 

                }
                break;
            case 34 :
                // InternalCalur.g:1:208: T__45
                {
                mT__45(); 

                }
                break;
            case 35 :
                // InternalCalur.g:1:214: T__46
                {
                mT__46(); 

                }
                break;
            case 36 :
                // InternalCalur.g:1:220: T__47
                {
                mT__47(); 

                }
                break;
            case 37 :
                // InternalCalur.g:1:226: T__48
                {
                mT__48(); 

                }
                break;
            case 38 :
                // InternalCalur.g:1:232: T__49
                {
                mT__49(); 

                }
                break;
            case 39 :
                // InternalCalur.g:1:238: T__50
                {
                mT__50(); 

                }
                break;
            case 40 :
                // InternalCalur.g:1:244: T__51
                {
                mT__51(); 

                }
                break;
            case 41 :
                // InternalCalur.g:1:250: T__52
                {
                mT__52(); 

                }
                break;
            case 42 :
                // InternalCalur.g:1:256: T__53
                {
                mT__53(); 

                }
                break;
            case 43 :
                // InternalCalur.g:1:262: T__54
                {
                mT__54(); 

                }
                break;
            case 44 :
                // InternalCalur.g:1:268: T__55
                {
                mT__55(); 

                }
                break;
            case 45 :
                // InternalCalur.g:1:274: T__56
                {
                mT__56(); 

                }
                break;
            case 46 :
                // InternalCalur.g:1:280: T__57
                {
                mT__57(); 

                }
                break;
            case 47 :
                // InternalCalur.g:1:286: T__58
                {
                mT__58(); 

                }
                break;
            case 48 :
                // InternalCalur.g:1:292: T__59
                {
                mT__59(); 

                }
                break;
            case 49 :
                // InternalCalur.g:1:298: T__60
                {
                mT__60(); 

                }
                break;
            case 50 :
                // InternalCalur.g:1:304: T__61
                {
                mT__61(); 

                }
                break;
            case 51 :
                // InternalCalur.g:1:310: T__62
                {
                mT__62(); 

                }
                break;
            case 52 :
                // InternalCalur.g:1:316: T__63
                {
                mT__63(); 

                }
                break;
            case 53 :
                // InternalCalur.g:1:322: T__64
                {
                mT__64(); 

                }
                break;
            case 54 :
                // InternalCalur.g:1:328: T__65
                {
                mT__65(); 

                }
                break;
            case 55 :
                // InternalCalur.g:1:334: T__66
                {
                mT__66(); 

                }
                break;
            case 56 :
                // InternalCalur.g:1:340: T__67
                {
                mT__67(); 

                }
                break;
            case 57 :
                // InternalCalur.g:1:346: T__68
                {
                mT__68(); 

                }
                break;
            case 58 :
                // InternalCalur.g:1:352: T__69
                {
                mT__69(); 

                }
                break;
            case 59 :
                // InternalCalur.g:1:358: T__70
                {
                mT__70(); 

                }
                break;
            case 60 :
                // InternalCalur.g:1:364: T__71
                {
                mT__71(); 

                }
                break;
            case 61 :
                // InternalCalur.g:1:370: T__72
                {
                mT__72(); 

                }
                break;
            case 62 :
                // InternalCalur.g:1:376: T__73
                {
                mT__73(); 

                }
                break;
            case 63 :
                // InternalCalur.g:1:382: T__74
                {
                mT__74(); 

                }
                break;
            case 64 :
                // InternalCalur.g:1:388: RULE_FLOAT
                {
                mRULE_FLOAT(); 

                }
                break;
            case 65 :
                // InternalCalur.g:1:399: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 66 :
                // InternalCalur.g:1:407: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 67 :
                // InternalCalur.g:1:416: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 68 :
                // InternalCalur.g:1:428: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 69 :
                // InternalCalur.g:1:444: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 70 :
                // InternalCalur.g:1:460: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 71 :
                // InternalCalur.g:1:468: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA12 dfa12 = new DFA12(this);
    static final String DFA12_eotS =
        "\1\uffff\2\50\1\55\1\57\1\uffff\5\50\3\uffff\1\103\1\uffff\3\50\1\uffff\3\50\1\122\2\50\1\127\4\uffff\1\134\1\45\1\uffff\2\45\2\uffff\2\50\1\uffff\2\50\6\uffff\5\50\1\154\4\50\1\166\1\50\1\170\7\uffff\1\50\1\172\5\50\1\uffff\4\50\2\uffff\3\50\7\uffff\1\134\3\uffff\1\u008b\10\50\1\u0094\1\u0095\1\uffff\11\50\1\uffff\1\50\1\uffff\1\50\1\uffff\6\50\1\u00a7\3\50\1\u00ab\5\50\1\uffff\1\u00b1\1\u00b2\2\50\1\u00b5\1\50\1\u00b7\1\50\2\uffff\1\u00b9\2\50\1\u00bc\6\50\1\u00c3\1\50\1\u00c5\4\50\1\uffff\1\u00ca\1\50\1\u00cc\1\uffff\1\50\1\u00ce\3\50\2\uffff\2\50\1\uffff\1\50\1\uffff\1\u00d5\1\uffff\2\50\1\uffff\1\u00d8\1\u00d9\1\u00da\3\50\3\uffff\4\50\1\uffff\1\50\1\uffff\1\u00e3\1\uffff\4\50\1\u00e8\1\50\1\uffff\1\50\1\u00eb\3\uffff\1\u00ec\1\50\1\u00ee\5\50\1\uffff\1\50\1\u00f5\2\50\1\uffff\1\u00f8\1\50\2\uffff\1\50\1\uffff\2\50\1\u00fe\2\50\1\u0101\1\uffff\1\50\1\u0103\1\uffff\5\50\1\uffff\1\u0109\1\50\1\uffff\1\50\1\uffff\2\50\1\u010e\2\50\1\uffff\4\50\1\uffff\2\50\1\u0117\1\u0118\1\u0119\3\50\3\uffff\1\50\1\u011e\1\u011f\1\50\2\uffff\1\50\1\u0122\1\uffff";
    static final String DFA12_eofS =
        "\u0123\uffff";
    static final String DFA12_minS =
        "\1\0\1\157\1\141\2\75\1\uffff\1\145\1\142\2\141\1\146\3\uffff\1\52\1\uffff\1\150\1\145\1\141\1\uffff\1\141\1\151\1\157\1\75\1\154\1\145\1\51\4\uffff\1\56\1\101\1\uffff\2\0\2\uffff\1\164\1\154\1\uffff\1\156\1\147\6\uffff\1\162\1\143\1\141\1\163\1\144\1\60\1\151\1\154\1\157\1\154\1\60\1\160\1\60\7\uffff\1\165\1\60\1\145\1\143\1\163\2\162\1\uffff\2\162\1\164\1\147\2\uffff\1\145\1\163\1\160\7\uffff\1\56\3\uffff\1\60\1\154\1\144\1\151\1\165\1\164\1\157\1\144\1\156\2\60\1\uffff\1\154\1\163\1\143\1\154\1\157\1\163\1\145\1\157\1\141\1\uffff\1\157\1\uffff\1\145\1\uffff\1\163\1\156\1\154\1\162\1\163\1\142\1\60\1\164\1\141\1\150\1\60\1\162\1\145\1\164\1\157\1\145\1\uffff\2\60\1\163\1\162\1\60\1\156\1\60\1\144\2\uffff\1\60\1\165\1\145\1\60\1\162\1\145\1\170\3\162\1\60\1\56\1\60\1\151\1\157\2\141\1\uffff\1\60\1\155\1\60\1\uffff\1\171\1\60\2\162\1\147\2\uffff\1\164\1\156\1\uffff\1\144\1\uffff\1\60\1\uffff\2\154\1\uffff\3\60\1\155\1\156\1\164\3\uffff\2\163\1\147\1\164\1\uffff\1\145\1\uffff\1\60\1\uffff\1\157\1\164\1\151\1\145\1\60\1\163\1\uffff\1\145\1\60\3\uffff\1\60\1\141\1\60\3\145\1\151\1\164\1\uffff\1\171\1\60\1\163\1\162\1\uffff\1\60\1\116\2\uffff\1\164\1\uffff\2\143\1\60\1\155\1\145\1\60\1\uffff\1\164\1\60\1\uffff\2\141\1\145\2\157\1\uffff\1\60\1\162\1\uffff\1\145\1\uffff\1\155\1\162\1\60\2\156\1\uffff\1\163\1\162\1\145\1\164\1\uffff\2\144\3\60\1\116\2\163\3\uffff\1\141\2\60\1\155\2\uffff\1\145\1\60\1\uffff";
    static final String DFA12_maxS =
        "\1\uffff\1\165\1\145\1\76\1\75\1\uffff\1\162\1\164\1\145\1\154\1\156\3\uffff\1\57\1\uffff\1\162\1\151\1\145\1\uffff\1\157\1\151\1\157\1\75\1\166\1\145\1\51\4\uffff\1\71\1\172\1\uffff\2\uffff\2\uffff\1\164\1\154\1\uffff\1\156\1\164\6\uffff\1\162\1\156\1\141\1\163\1\144\1\172\1\151\1\160\1\157\1\154\1\172\1\160\1\172\7\uffff\1\165\1\172\1\151\1\154\1\163\2\162\1\uffff\2\162\1\164\1\147\2\uffff\1\145\2\163\7\uffff\1\71\3\uffff\1\172\1\154\1\144\1\151\1\165\1\164\1\157\1\144\1\156\2\172\1\uffff\1\154\1\163\1\143\1\154\1\157\1\163\1\145\1\157\1\141\1\uffff\1\157\1\uffff\1\145\1\uffff\1\163\1\156\1\154\1\162\1\163\1\142\1\172\1\164\1\141\1\150\1\172\1\162\1\145\1\164\1\157\1\145\1\uffff\2\172\1\163\1\162\1\172\1\156\1\172\1\144\2\uffff\1\172\1\165\1\145\1\172\1\162\1\145\1\170\3\162\1\172\1\56\1\172\1\151\1\157\2\141\1\uffff\1\172\1\155\1\172\1\uffff\1\171\1\172\2\162\1\147\2\uffff\1\164\1\156\1\uffff\1\144\1\uffff\1\172\1\uffff\2\154\1\uffff\3\172\1\155\1\156\1\164\3\uffff\2\163\1\147\1\164\1\uffff\1\145\1\uffff\1\172\1\uffff\1\157\1\164\1\151\1\145\1\172\1\163\1\uffff\1\145\1\172\3\uffff\1\172\1\141\1\172\3\145\1\151\1\164\1\uffff\1\171\1\172\1\163\1\162\1\uffff\1\172\1\120\2\uffff\1\164\1\uffff\2\143\1\172\1\155\1\145\1\172\1\uffff\1\164\1\172\1\uffff\2\141\1\145\2\157\1\uffff\1\172\1\162\1\uffff\1\145\1\uffff\1\155\1\162\1\172\2\156\1\uffff\1\163\1\162\1\145\1\164\1\uffff\2\144\3\172\1\116\2\163\3\uffff\1\141\2\172\1\155\2\uffff\1\145\1\172\1\uffff";
    static final String DFA12_acceptS =
        "\5\uffff\1\7\5\uffff\1\20\1\21\1\22\1\uffff\1\24\3\uffff\1\33\7\uffff\1\61\1\70\1\73\1\74\2\uffff\1\101\2\uffff\1\106\1\107\2\uffff\1\101\2\uffff\1\5\1\10\1\3\1\6\1\4\1\7\15\uffff\1\20\1\21\1\22\1\104\1\105\1\23\1\24\7\uffff\1\33\4\uffff\1\66\1\45\3\uffff\1\76\1\60\1\61\1\70\1\73\1\74\1\102\1\uffff\1\100\1\103\1\106\13\uffff\1\47\11\uffff\1\44\1\uffff\1\71\1\uffff\1\36\20\uffff\1\1\10\uffff\1\12\1\46\21\uffff\1\62\3\uffff\1\42\5\uffff\1\77\1\2\2\uffff\1\11\1\uffff\1\34\1\uffff\1\13\2\uffff\1\63\6\uffff\1\25\1\67\1\72\4\uffff\1\37\1\uffff\1\40\1\uffff\1\75\6\uffff\1\65\2\uffff\1\14\1\26\1\17\10\uffff\1\50\4\uffff\1\64\2\uffff\1\51\1\43\1\uffff\1\54\6\uffff\1\55\2\uffff\1\27\5\uffff\1\35\2\uffff\1\53\1\uffff\1\56\5\uffff\1\32\4\uffff\1\52\10\uffff\1\41\1\57\1\15\4\uffff\1\30\1\31\2\uffff\1\16";
    static final String DFA12_specialS =
        "\1\1\41\uffff\1\0\1\2\u00ff\uffff}>";
    static final String[] DFA12_transitionS = {
            "\11\45\2\44\2\45\1\44\22\45\1\44\1\45\1\42\2\45\1\17\1\45\1\43\1\32\1\33\1\15\1\13\1\45\1\14\1\34\1\16\12\37\1\27\1\23\1\3\1\5\1\4\2\45\32\41\3\45\1\40\1\41\1\45\1\7\1\41\1\10\1\31\1\30\1\11\2\41\1\12\2\41\1\26\1\21\1\1\1\41\1\24\1\41\1\2\1\6\1\20\1\41\1\22\1\25\3\41\1\35\1\45\1\36\uff82\45",
            "\1\46\5\uffff\1\47",
            "\1\51\3\uffff\1\52",
            "\1\53\1\54",
            "\1\56",
            "",
            "\1\62\13\uffff\1\61\1\63",
            "\1\64\13\uffff\1\65\5\uffff\1\66",
            "\1\70\3\uffff\1\67",
            "\1\72\12\uffff\1\71",
            "\1\75\6\uffff\1\74\1\73",
            "",
            "",
            "",
            "\1\101\4\uffff\1\102",
            "",
            "\1\107\6\uffff\1\106\2\uffff\1\105",
            "\1\111\3\uffff\1\110",
            "\1\113\3\uffff\1\112",
            "",
            "\1\116\15\uffff\1\115",
            "\1\117",
            "\1\120",
            "\1\121",
            "\1\124\11\uffff\1\123",
            "\1\125",
            "\1\126",
            "",
            "",
            "",
            "",
            "\1\136\1\uffff\12\135",
            "\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "\0\137",
            "\0\137",
            "",
            "",
            "\1\141",
            "\1\142",
            "",
            "\1\143",
            "\1\144\14\uffff\1\145",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\146",
            "\1\147\12\uffff\1\150",
            "\1\151",
            "\1\152",
            "\1\153",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\155",
            "\1\160\1\uffff\1\157\1\uffff\1\156",
            "\1\161",
            "\1\162",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\2\50\1\165\1\163\1\50\1\164\24\50",
            "\1\167",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\171",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\174\3\uffff\1\173",
            "\1\176\10\uffff\1\175",
            "\1\177",
            "\1\u0080",
            "\1\u0081",
            "",
            "\1\u0082",
            "\1\u0083",
            "\1\u0084",
            "\1\u0085",
            "",
            "",
            "\1\u0086",
            "\1\u0087",
            "\1\u0089\1\uffff\1\u008a\1\u0088",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\136\1\uffff\12\135",
            "",
            "",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u008c",
            "\1\u008d",
            "\1\u008e",
            "\1\u008f",
            "\1\u0090",
            "\1\u0091",
            "\1\u0092",
            "\1\u0093",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "\1\u0096",
            "\1\u0097",
            "\1\u0098",
            "\1\u0099",
            "\1\u009a",
            "\1\u009b",
            "\1\u009c",
            "\1\u009d",
            "\1\u009e",
            "",
            "\1\u009f",
            "",
            "\1\u00a0",
            "",
            "\1\u00a1",
            "\1\u00a2",
            "\1\u00a3",
            "\1\u00a4",
            "\1\u00a5",
            "\1\u00a6",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00a8",
            "\1\u00a9",
            "\1\u00aa",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00ac",
            "\1\u00ad",
            "\1\u00ae",
            "\1\u00af",
            "\1\u00b0",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00b3",
            "\1\u00b4",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00b6",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00b8",
            "",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00ba",
            "\1\u00bb",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00bd",
            "\1\u00be",
            "\1\u00bf",
            "\1\u00c0",
            "\1\u00c1",
            "\1\u00c2",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00c4",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00c6",
            "\1\u00c7",
            "\1\u00c8",
            "\1\u00c9",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00cb",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "\1\u00cd",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00cf",
            "\1\u00d0",
            "\1\u00d1",
            "",
            "",
            "\1\u00d2",
            "\1\u00d3",
            "",
            "\1\u00d4",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "\1\u00d6",
            "\1\u00d7",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00db",
            "\1\u00dc",
            "\1\u00dd",
            "",
            "",
            "",
            "\1\u00de",
            "\1\u00df",
            "\1\u00e0",
            "\1\u00e1",
            "",
            "\1\u00e2",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "\1\u00e4",
            "\1\u00e5",
            "\1\u00e6",
            "\1\u00e7",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00e9",
            "",
            "\1\u00ea",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00ed",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00ef",
            "\1\u00f0",
            "\1\u00f1",
            "\1\u00f2",
            "\1\u00f3",
            "",
            "\1\u00f4",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00f6",
            "\1\u00f7",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00f9\1\uffff\1\u00fa",
            "",
            "",
            "\1\u00fb",
            "",
            "\1\u00fc",
            "\1\u00fd",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u00ff",
            "\1\u0100",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "\1\u0102",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "",
            "\1\u0104",
            "\1\u0105",
            "\1\u0106",
            "\1\u0107",
            "\1\u0108",
            "",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u010a",
            "",
            "\1\u010b",
            "",
            "\1\u010c",
            "\1\u010d",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u010f",
            "\1\u0110",
            "",
            "\1\u0111",
            "\1\u0112",
            "\1\u0113",
            "\1\u0114",
            "",
            "\1\u0115",
            "\1\u0116",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u011a",
            "\1\u011b",
            "\1\u011c",
            "",
            "",
            "",
            "\1\u011d",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            "\1\u0120",
            "",
            "",
            "\1\u0121",
            "\12\50\7\uffff\32\50\4\uffff\1\50\1\uffff\32\50",
            ""
    };

    static final short[] DFA12_eot = DFA.unpackEncodedString(DFA12_eotS);
    static final short[] DFA12_eof = DFA.unpackEncodedString(DFA12_eofS);
    static final char[] DFA12_min = DFA.unpackEncodedStringToUnsignedChars(DFA12_minS);
    static final char[] DFA12_max = DFA.unpackEncodedStringToUnsignedChars(DFA12_maxS);
    static final short[] DFA12_accept = DFA.unpackEncodedString(DFA12_acceptS);
    static final short[] DFA12_special = DFA.unpackEncodedString(DFA12_specialS);
    static final short[][] DFA12_transition;

    static {
        int numStates = DFA12_transitionS.length;
        DFA12_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA12_transition[i] = DFA.unpackEncodedString(DFA12_transitionS[i]);
        }
    }

    class DFA12 extends DFA {

        public DFA12(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 12;
            this.eot = DFA12_eot;
            this.eof = DFA12_eof;
            this.min = DFA12_min;
            this.max = DFA12_max;
            this.accept = DFA12_accept;
            this.special = DFA12_special;
            this.transition = DFA12_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | T__69 | T__70 | T__71 | T__72 | T__73 | T__74 | RULE_FLOAT | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA12_34 = input.LA(1);

                        s = -1;
                        if ( ((LA12_34>='\u0000' && LA12_34<='\uFFFF')) ) {s = 95;}

                        else s = 37;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA12_0 = input.LA(1);

                        s = -1;
                        if ( (LA12_0=='n') ) {s = 1;}

                        else if ( (LA12_0=='r') ) {s = 2;}

                        else if ( (LA12_0=='<') ) {s = 3;}

                        else if ( (LA12_0=='>') ) {s = 4;}

                        else if ( (LA12_0=='=') ) {s = 5;}

                        else if ( (LA12_0=='s') ) {s = 6;}

                        else if ( (LA12_0=='a') ) {s = 7;}

                        else if ( (LA12_0=='c') ) {s = 8;}

                        else if ( (LA12_0=='f') ) {s = 9;}

                        else if ( (LA12_0=='i') ) {s = 10;}

                        else if ( (LA12_0=='+') ) {s = 11;}

                        else if ( (LA12_0=='-') ) {s = 12;}

                        else if ( (LA12_0=='*') ) {s = 13;}

                        else if ( (LA12_0=='/') ) {s = 14;}

                        else if ( (LA12_0=='%') ) {s = 15;}

                        else if ( (LA12_0=='t') ) {s = 16;}

                        else if ( (LA12_0=='m') ) {s = 17;}

                        else if ( (LA12_0=='v') ) {s = 18;}

                        else if ( (LA12_0==';') ) {s = 19;}

                        else if ( (LA12_0=='p') ) {s = 20;}

                        else if ( (LA12_0=='w') ) {s = 21;}

                        else if ( (LA12_0=='l') ) {s = 22;}

                        else if ( (LA12_0==':') ) {s = 23;}

                        else if ( (LA12_0=='e') ) {s = 24;}

                        else if ( (LA12_0=='d') ) {s = 25;}

                        else if ( (LA12_0=='(') ) {s = 26;}

                        else if ( (LA12_0==')') ) {s = 27;}

                        else if ( (LA12_0=='.') ) {s = 28;}

                        else if ( (LA12_0=='{') ) {s = 29;}

                        else if ( (LA12_0=='}') ) {s = 30;}

                        else if ( ((LA12_0>='0' && LA12_0<='9')) ) {s = 31;}

                        else if ( (LA12_0=='^') ) {s = 32;}

                        else if ( ((LA12_0>='A' && LA12_0<='Z')||LA12_0=='_'||LA12_0=='b'||(LA12_0>='g' && LA12_0<='h')||(LA12_0>='j' && LA12_0<='k')||LA12_0=='o'||LA12_0=='q'||LA12_0=='u'||(LA12_0>='x' && LA12_0<='z')) ) {s = 33;}

                        else if ( (LA12_0=='\"') ) {s = 34;}

                        else if ( (LA12_0=='\'') ) {s = 35;}

                        else if ( ((LA12_0>='\t' && LA12_0<='\n')||LA12_0=='\r'||LA12_0==' ') ) {s = 36;}

                        else if ( ((LA12_0>='\u0000' && LA12_0<='\b')||(LA12_0>='\u000B' && LA12_0<='\f')||(LA12_0>='\u000E' && LA12_0<='\u001F')||LA12_0=='!'||(LA12_0>='#' && LA12_0<='$')||LA12_0=='&'||LA12_0==','||(LA12_0>='?' && LA12_0<='@')||(LA12_0>='[' && LA12_0<=']')||LA12_0=='`'||LA12_0=='|'||(LA12_0>='~' && LA12_0<='\uFFFF')) ) {s = 37;}

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA12_35 = input.LA(1);

                        s = -1;
                        if ( ((LA12_35>='\u0000' && LA12_35<='\uFFFF')) ) {s = 95;}

                        else s = 37;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 12, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}