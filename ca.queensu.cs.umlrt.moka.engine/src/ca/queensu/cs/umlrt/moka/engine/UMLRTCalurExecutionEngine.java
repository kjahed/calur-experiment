package ca.queensu.cs.umlrt.moka.engine;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.papyrus.moka.composites.CompositeStructuresExecutionEngine;
import org.eclipse.papyrus.moka.composites.Semantics.impl.Loci.LociL3.CS_Executor;
import org.eclipse.papyrus.moka.fuml.Semantics.Loci.LociL1.ILocus;
import org.eclipse.papyrusrt.umlrt.core.utils.CapsuleUtils;
import org.eclipse.uml2.uml.Classifier;

import ca.queensu.cs.umlrt.moka.engine.semantics.Loci.Calur_ExecutionFactory;
import ca.queensu.cs.umlrt.moka.engine.semantics.Loci.Calur_Locus;
import ca.queensu.cs.umlrt.moka.engine.semantics.StateMachines.NextEventStrategy;

public class UMLRTCalurExecutionEngine  extends CompositeStructuresExecutionEngine {

	@Override
	public ILocus initializeLocus() {
		ILocus locus = new Calur_Locus();
		locus.setExecutor(new CS_Executor());
		locus.setFactory(new Calur_ExecutionFactory());
		return locus;
	}
	
	@Override
	public void start(IProgressMonitor monitor) {
		this.locus = this.initializeLocus();
		if (this.executionEntryPoint != null &&
			this.executionEntryPoint instanceof Classifier &&
			CapsuleUtils.isCapsule(((Classifier)this.executionEntryPoint))) {
			// initializes built-in primitive types
			this.initializeBuiltInPrimitiveTypes(locus);
			// Initializes opaque behavior executions
			this.registerOpaqueBehaviorExecutions(locus);
			// Initializes system services
			this.registerSystemServices(locus);
			// Initializes semantic strategies
			this.registerSemanticStrategies(locus);
			// Initializes arguments
			//this.initializeArguments(this.executionArgs);
			// Finally launches the execution$
			//this.locus.getExecutor().execute(((org.eclipse.uml2.uml.Class)this.executionEntryPoint).getOwnedBehaviors().get(0), null, this.executionArguments);
			this.locus.getExecutor().execute(((org.eclipse.uml2.uml.Class)this.executionEntryPoint).getOwnedBehaviors().get(0), null, this.executionArguments);
			return;
		}
		super.start(monitor);
	}
	
	// Register semantic strategies available in the environment
	@Override
	protected void registerSemanticStrategies(ILocus locus) {
		super.registerSemanticStrategies(locus);
		locus.getFactory().setStrategy(new NextEventStrategy());
	}
}
