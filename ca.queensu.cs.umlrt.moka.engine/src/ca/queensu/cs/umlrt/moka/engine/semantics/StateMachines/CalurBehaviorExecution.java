package ca.queensu.cs.umlrt.moka.engine.semantics.StateMachines;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.moka.fuml.Semantics.Classes.Kernel.IValue;
import org.eclipse.papyrus.moka.fuml.Semantics.CommonBehaviors.BasicBehaviors.IParameterValue;
import org.eclipse.papyrus.moka.fuml.Semantics.impl.Classes.Kernel.RealValue;
import org.eclipse.papyrus.moka.fuml.Semantics.impl.CommonBehaviors.BasicBehaviors.OpaqueBehaviorExecution;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.PrimitiveType;

public class CalurBehaviorExecution extends OpaqueBehaviorExecution {

	protected OpaqueBehavior opaqueBehavior;

	protected Behavior behaviorSignature;
	
	public CalurBehaviorExecution(OpaqueBehavior opaqueBehavior) {
		super();
		this.opaqueBehavior = opaqueBehavior;
		this.addType(opaqueBehavior);
	}
	
/*	@Override
	public Behavior getBehavior() {
		return super.getBehavior();
		// TODO Auto-generated method stub
		//return opaqueBehavior;
	} */
	
	@Override
	public void doBody(List<IParameterValue> inputParameters, List<IParameterValue> outputParameters) {
		// TODO Auto-generated method stub
		System.out.println("doing something");
	/*	Double x = 10.1;
		RealValue result = new RealValue();
		result.value = Math.abs(x);
		result.type = (PrimitiveType) this.locus.getFactory().getBuiltInType("Real");
		List<IValue> outputs = new ArrayList<IValue>();
		outputs.add(result);
		//outputParameters.get(0).setValues(outputs); */
	}

	@Override
	public IValue new_() {
		return new CalurBehaviorExecution(opaqueBehavior);
	}

}
