package ca.queensu.cs.umlrt.calur;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.xtext.linking.lazy.SyntheticLinkingSupport;
import org.eclipse.xtext.resource.DerivedStateAwareResource;
import org.eclipse.xtext.resource.IDerivedStateComputer;

import com.google.inject.Inject;

import ca.queensu.cs.umlrt.calur.calur.Action;
import ca.queensu.cs.umlrt.calur.calur.CalurPackage;

public class CalurDerivedStateComputer implements IDerivedStateComputer {

	@Inject
	SyntheticLinkingSupport sls;
	
	NamedElement currentSelectedEObject;
	
	String currentSelectedFeatureName; // Can be Entry, Exit, Guard, Effect, ...
	
	@Inject
	public void getEObjectHandler(CurrentSelectedEObjectHandler handler) {
		currentSelectedEObject = (NamedElement) handler.getSelection();
		currentSelectedFeatureName = handler.getTabName();
	}
		
	@Override
	public void installDerivedState(DerivedStateAwareResource resource, boolean preLinkingPhase) {
		if (!resource.getContents().isEmpty() && currentSelectedEObject != null) {
			EObject obj = resource.getContents().get(0);
			
//			sls.createAndSetProxy(obj, CalurPackage.Literals.ACTION__CONTAINER, (currentSelectedEObject.getQualifiedName()));
//			sls.
			((Action)obj).setContainer(currentSelectedEObject);
			((Action)obj).setFeatureName(currentSelectedFeatureName);
		}
	}

	@Override
	public void discardDerivedState(DerivedStateAwareResource resource) {
	}

}
