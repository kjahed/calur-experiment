package ca.queensu.cs.umlrt.calur;

import org.eclipse.emf.ecore.EObject;

import com.google.inject.Singleton;

@Singleton
public class CurrentSelectedEObjectHandler {

	
	private static EObject selection;
	private static String tabName;

	public EObject getSelection() {
		return selection;
	}

	public void setSelection(EObject sel) {
		selection = sel;
	}

	public void setTabName(String currentSelectedTabName) {
		tabName = currentSelectedTabName;
	}
	
	public String getTabName() {
		return tabName;
	}
	
}
