/**
 * generated by Xtext 2.14.0
 */
package ca.queensu.cs.umlrt.calur.calur;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable Ref</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ca.queensu.cs.umlrt.calur.calur.VariableRef#getSegments <em>Segments</em>}</li>
 * </ul>
 *
 * @see ca.queensu.cs.umlrt.calur.calur.CalurPackage#getVariableRef()
 * @model
 * @generated
 */
public interface VariableRef extends BinaryExpr
{
  /**
   * Returns the value of the '<em><b>Segments</b></em>' containment reference list.
   * The list contents are of type {@link ca.queensu.cs.umlrt.calur.calur.Field}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Segments</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Segments</em>' containment reference list.
   * @see ca.queensu.cs.umlrt.calur.calur.CalurPackage#getVariableRef_Segments()
   * @model containment="true"
   * @generated
   */
  EList<Field> getSegments();

} // VariableRef
