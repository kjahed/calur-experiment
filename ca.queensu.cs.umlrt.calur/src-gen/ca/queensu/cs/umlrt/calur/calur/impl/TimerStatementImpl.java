/**
 * generated by Xtext 2.14.0
 */
package ca.queensu.cs.umlrt.calur.calur.impl;

import ca.queensu.cs.umlrt.calur.calur.CalurPackage;
import ca.queensu.cs.umlrt.calur.calur.TimeSpec;
import ca.queensu.cs.umlrt.calur.calur.TimerStatement;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Timer Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.queensu.cs.umlrt.calur.calur.impl.TimerStatementImpl#getTimerId <em>Timer Id</em>}</li>
 *   <li>{@link ca.queensu.cs.umlrt.calur.calur.impl.TimerStatementImpl#getTimeSpec <em>Time Spec</em>}</li>
 *   <li>{@link ca.queensu.cs.umlrt.calur.calur.impl.TimerStatementImpl#getPort <em>Port</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TimerStatementImpl extends StatementImpl implements TimerStatement
{
  /**
   * The cached value of the '{@link #getTimerId() <em>Timer Id</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTimerId()
   * @generated
   * @ordered
   */
  protected Property timerId;

  /**
   * The cached value of the '{@link #getTimeSpec() <em>Time Spec</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTimeSpec()
   * @generated
   * @ordered
   */
  protected EList<TimeSpec> timeSpec;

  /**
   * The cached value of the '{@link #getPort() <em>Port</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPort()
   * @generated
   * @ordered
   */
  protected Port port;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TimerStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CalurPackage.Literals.TIMER_STATEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Property getTimerId()
  {
    if (timerId != null && timerId.eIsProxy())
    {
      InternalEObject oldTimerId = (InternalEObject)timerId;
      timerId = (Property)eResolveProxy(oldTimerId);
      if (timerId != oldTimerId)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CalurPackage.TIMER_STATEMENT__TIMER_ID, oldTimerId, timerId));
      }
    }
    return timerId;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Property basicGetTimerId()
  {
    return timerId;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTimerId(Property newTimerId)
  {
    Property oldTimerId = timerId;
    timerId = newTimerId;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CalurPackage.TIMER_STATEMENT__TIMER_ID, oldTimerId, timerId));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<TimeSpec> getTimeSpec()
  {
    if (timeSpec == null)
    {
      timeSpec = new EObjectContainmentEList<TimeSpec>(TimeSpec.class, this, CalurPackage.TIMER_STATEMENT__TIME_SPEC);
    }
    return timeSpec;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Port getPort()
  {
    if (port != null && port.eIsProxy())
    {
      InternalEObject oldPort = (InternalEObject)port;
      port = (Port)eResolveProxy(oldPort);
      if (port != oldPort)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, CalurPackage.TIMER_STATEMENT__PORT, oldPort, port));
      }
    }
    return port;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Port basicGetPort()
  {
    return port;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPort(Port newPort)
  {
    Port oldPort = port;
    port = newPort;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CalurPackage.TIMER_STATEMENT__PORT, oldPort, port));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case CalurPackage.TIMER_STATEMENT__TIME_SPEC:
        return ((InternalEList<?>)getTimeSpec()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CalurPackage.TIMER_STATEMENT__TIMER_ID:
        if (resolve) return getTimerId();
        return basicGetTimerId();
      case CalurPackage.TIMER_STATEMENT__TIME_SPEC:
        return getTimeSpec();
      case CalurPackage.TIMER_STATEMENT__PORT:
        if (resolve) return getPort();
        return basicGetPort();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CalurPackage.TIMER_STATEMENT__TIMER_ID:
        setTimerId((Property)newValue);
        return;
      case CalurPackage.TIMER_STATEMENT__TIME_SPEC:
        getTimeSpec().clear();
        getTimeSpec().addAll((Collection<? extends TimeSpec>)newValue);
        return;
      case CalurPackage.TIMER_STATEMENT__PORT:
        setPort((Port)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CalurPackage.TIMER_STATEMENT__TIMER_ID:
        setTimerId((Property)null);
        return;
      case CalurPackage.TIMER_STATEMENT__TIME_SPEC:
        getTimeSpec().clear();
        return;
      case CalurPackage.TIMER_STATEMENT__PORT:
        setPort((Port)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CalurPackage.TIMER_STATEMENT__TIMER_ID:
        return timerId != null;
      case CalurPackage.TIMER_STATEMENT__TIME_SPEC:
        return timeSpec != null && !timeSpec.isEmpty();
      case CalurPackage.TIMER_STATEMENT__PORT:
        return port != null;
    }
    return super.eIsSet(featureID);
  }

} //TimerStatementImpl
