/**
 * generated by Xtext 2.14.0
 */
package ca.queensu.cs.umlrt.calur.calur.impl;

import ca.queensu.cs.umlrt.calur.calur.CalurPackage;
import ca.queensu.cs.umlrt.calur.calur.InformEveryTimerStatement;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Inform Every Timer Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InformEveryTimerStatementImpl extends TimerStatementImpl implements InformEveryTimerStatement
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected InformEveryTimerStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CalurPackage.Literals.INFORM_EVERY_TIMER_STATEMENT;
  }

} //InformEveryTimerStatementImpl
