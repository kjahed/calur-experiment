/**
 * generated by Xtext 2.14.0
 */
package ca.queensu.cs.umlrt.calur.calur.impl;

import ca.queensu.cs.umlrt.calur.calur.CalurPackage;
import ca.queensu.cs.umlrt.calur.calur.LiteralNull;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Literal Null</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class LiteralNullImpl extends ValueSpecificationImpl implements LiteralNull
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LiteralNullImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CalurPackage.Literals.LITERAL_NULL;
  }

} //LiteralNullImpl
