/**
 * generated by Xtext 2.14.0
 */
package ca.queensu.cs.umlrt.calur.calur.impl;

import ca.queensu.cs.umlrt.calur.calur.CallStatement;
import ca.queensu.cs.umlrt.calur.calur.CalurPackage;
import ca.queensu.cs.umlrt.calur.calur.VariableRef;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Call Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ca.queensu.cs.umlrt.calur.calur.impl.CallStatementImpl#getVarRef <em>Var Ref</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CallStatementImpl extends StatementImpl implements CallStatement
{
  /**
   * The cached value of the '{@link #getVarRef() <em>Var Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVarRef()
   * @generated
   * @ordered
   */
  protected VariableRef varRef;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CallStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return CalurPackage.Literals.CALL_STATEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariableRef getVarRef()
  {
    return varRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetVarRef(VariableRef newVarRef, NotificationChain msgs)
  {
    VariableRef oldVarRef = varRef;
    varRef = newVarRef;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CalurPackage.CALL_STATEMENT__VAR_REF, oldVarRef, newVarRef);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVarRef(VariableRef newVarRef)
  {
    if (newVarRef != varRef)
    {
      NotificationChain msgs = null;
      if (varRef != null)
        msgs = ((InternalEObject)varRef).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CalurPackage.CALL_STATEMENT__VAR_REF, null, msgs);
      if (newVarRef != null)
        msgs = ((InternalEObject)newVarRef).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CalurPackage.CALL_STATEMENT__VAR_REF, null, msgs);
      msgs = basicSetVarRef(newVarRef, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, CalurPackage.CALL_STATEMENT__VAR_REF, newVarRef, newVarRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case CalurPackage.CALL_STATEMENT__VAR_REF:
        return basicSetVarRef(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case CalurPackage.CALL_STATEMENT__VAR_REF:
        return getVarRef();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case CalurPackage.CALL_STATEMENT__VAR_REF:
        setVarRef((VariableRef)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case CalurPackage.CALL_STATEMENT__VAR_REF:
        setVarRef((VariableRef)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case CalurPackage.CALL_STATEMENT__VAR_REF:
        return varRef != null;
    }
    return super.eIsSet(featureID);
  }

} //CallStatementImpl
